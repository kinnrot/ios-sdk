//
//  ShakeSensor.swift
//  Pods
//
//  Created by Anton Zagrebelny on 02/01/2017.
//
//
import Foundation
import CoreMotion

class ShakeSensor {
    private let _motionManager : CMMotionManager
    
    private let _deviceMotionUpdateInterval = 0.02
    private let _minimumForce = 5.2
    private let _minDirectionChange = 3
    private let _maxPauseBetweenDireactionChangeMs = 200
    private let _maxTotalDurationOfShakeMs = 1500
    
    private var _onDeviceShaken :() -> Void = {}
    
    private var _firstDirectionChangeTime : Date?
    private var _lastDirectionChangeTime : Date?
    private var _directionChangeCount = 0
    
    private var _lastX = 0.0
    private var _lastY = 0.0
    private var _lastZ = 0.0
    
    public init() {
        _motionManager = CMMotionManager()
        _motionManager.deviceMotionUpdateInterval = _deviceMotionUpdateInterval
    }
    
    public func start() {
        Logger.info(message: "Shake sensor started")
        _motionManager.startDeviceMotionUpdates(to: OperationQueue.main, withHandler: self.onMotionUpdate)
    }
    
    public func stop() {
        Logger.info(message: "Shake sensor stopped")
        _motionManager.stopDeviceMotionUpdates()
    }
    
    public func subscribeDeviceShaken(onDeviceShaken: @escaping () -> Void){
        _onDeviceShaken = onDeviceShaken
    }
    
    private func onMotionUpdate(motion: CMDeviceMotion?, error: Error?){
        let acceleration = motion!.userAcceleration
        let x = acceleration.x
        let y = acceleration.y
        let z = acceleration.z
        
        let totalMovement = abs(x + y + z - _lastX - _lastY - _lastZ);
        
        if (totalMovement > _minimumForce) {
            let now = Date();
            
            
            if _firstDirectionChangeTime == nil {
                _firstDirectionChangeTime = now;
                _lastDirectionChangeTime = now;
            }
            
            let fromLastChangeMs = Dates.durationMs(fromTime: _lastDirectionChangeTime!, toTime: now)
            
            if (fromLastChangeMs < _maxPauseBetweenDireactionChangeMs) {
                _lastDirectionChangeTime = now
                _directionChangeCount += 1
                
                _lastX = x
                _lastY = y
                _lastZ = z
                
                if _directionChangeCount >= _minDirectionChange {
                    let totalDuration = Dates.durationMs(fromTime: _firstDirectionChangeTime!, toTime: now)
                    
                    if (totalDuration < _maxTotalDurationOfShakeMs) {
                        Logger.info(message: "A SHAKE!")
                        self._onDeviceShaken()
                        resetSensorState()
                    }
                }
            }
            else {
                resetSensorState()
            }
        }
    }
    
    private func resetSensorState(){
        _firstDirectionChangeTime = nil
        _lastDirectionChangeTime = nil
        _directionChangeCount = 0
        _lastX = 0.0
        _lastY = 0.0
        _lastZ = 0.0
    }
}
