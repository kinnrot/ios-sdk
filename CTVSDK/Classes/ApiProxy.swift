//
//  ApiProxy.swift
//  Pods
//
//  Created by Anton Zagrebelny on 26/01/2017.
//
//

import Foundation

class ApiProxy {
    private var _apiBaseUrl: URL!;
    
    private let _operationQueue = OperationQueue()
    
    public init(){
        if let apiBaseUrl = Bundle.main.infoDictionary!["CTVApiBaseUrl"] as? String{
            _apiBaseUrl = URL(string:apiBaseUrl)
        }
        else{
            _apiBaseUrl = URL(string: "https://ctvmsdks.herokuapp.com/api/")
        }
    }
    
    public func subscribeDevice(appId: String!, existingDeviceId: String?, deviceToken: String!, deviceInfo: DeviceInfo, callback: @escaping (_: String) -> Void) {
        let request = newRequest(endpoint: "devices")
        request.httpMethod = "POST"
        
        let requestPayload = [
            "sdkVersion": CTVSDK.sdkVersion,
            "appId": appId,
            "deviceId": existingDeviceId,
            "platform": "IOS",
            "token": deviceToken,
            "adId": deviceInfo.adId,
            "deviceModel": deviceInfo.modelName,
            "deviceOs": deviceInfo.osVersion,
            "countryCode": deviceInfo.countryCode
            ] as [String : Any]
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: requestPayload, options: .prettyPrinted)
        } catch let error {
            Logger.error(message: "Failed to subscribe device due to serialization error.", error: error)
            return
        }
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: _operationQueue) { (response: URLResponse?, data: Data?, error: Error?) in
            if let anError = error {
                Logger.error(message: "Failed to subscribe device.", error: anError)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if (httpResponse.statusCode >= 400) {
                    Logger.error(message: "Failed to subscribe device. response status: \(httpResponse.statusCode)")
                    return
                }
                
                self.syncServerTime(httpResponse: httpResponse)
                
                if let respData = data {
                    do {
                        let respContent = try JSONSerialization.jsonObject(with: respData, options: .mutableContainers) as! Dictionary<String, String>
                        let deviceId = respContent["deviceId"]
                        callback(deviceId!)
                    } catch let error  {
                        Logger.error(message: "Failed to deserialize subscribe device response.", error: error)
                        return
                    }
                }
            }
        }
    }
    
    public func sendFeedback(appId: String!, deviceId: String!, adBroadcastId : String!, feedbackKind: FeedbackKind!){
        let request = newRequest(endpoint: "feedbacks")
        request.httpMethod = "POST"
        
        let requestPayload = [
            "deviceId": deviceId,
            "appId": appId,
            "adBroadcastId": adBroadcastId,
            "feedbackKind": feedbackKind.rawValue,
            "occurredAt": Int(Date().timeIntervalSince1970)
            ] as [String : Any]
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: requestPayload, options: .prettyPrinted)
        } catch let error {
            Logger.error(message: "Failed to send feedback due to serialization error.", error: error)
            return
        }
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: _operationQueue) { (response: URLResponse?, data: Data?, error: Error?) in
            if let anError = error {
                Logger.error(message: "Failed to send feedback.", error: error)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if (httpResponse.statusCode >= 400) {
                    Logger.error(message: "Failed to send feedback. response status: \(httpResponse.statusCode)")
                    return
                }
                
                self.syncServerTime(httpResponse: httpResponse)
            }
        }
        
    }
    
    private func newRequest(endpoint: String) -> MutableURLRequest {
        let url = URL(string: endpoint, relativeTo: _apiBaseUrl)
        let request = MutableURLRequest(url: url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        return request;
    }
    
    private func syncServerTime(httpResponse : HTTPURLResponse){
        let keyValues = httpResponse.allHeaderFields.map { (String(describing: $0.key).lowercased(), String(describing: $0.value)) }

        if let serverTimeHeader = keyValues.filter({ $0.0.lowercased() == "X-Server-Time".lowercased() }).first {
            let serverTime = Date.init(timeIntervalSince1970: Double(serverTimeHeader.1)!)
            ServerTime.setTime(time: serverTime)
        }
    }
}

enum FeedbackKind:String {
    case notificationReceived = "NOTIFICATION_RECEIVED"
    case deviceShaken = "DEVICE_SHAKEN"
    case actionTriggered = "ACTION_TRIGGERED"
    case adBroadcastExpired = "AD_BROADCAST_EXPIRED"
}


