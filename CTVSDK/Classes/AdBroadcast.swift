//
//  AdBroadcast.swift
//  Pods
//
//  Created by Anton Zagrebelny on 03/01/2017.
//
//

import Foundation
import ObjectMapper

class AdBroadcast : Mappable{
    var adBroadcastId : String!
    var adId : String!
    var adStartTime: Date!
    var adDurationSeconds: Int!
    var campaignId: String!
    
    var actionType: String!
    var webPageUrl : String?
    var appStoreUrl: String?
    
    var notificationTitle: String!
    var notificationSubtitle: String?
    var notificationBody: String!
    
    required init?(map: Map){
    }
    
    init(){
    }
    
    func getAdEndTime() -> Date {
        return adStartTime.addingTimeInterval(TimeInterval.init(adDurationSeconds))
    }
    
    func mapping(map: Map) {
        adBroadcastId <- map["adBroadcastId"]
        adId <- map["adId"]
        adStartTime <- (map["adStartTime"], DateTransform())
        adDurationSeconds <- map["adDurationSeconds"]
        campaignId <- map["campaignId"]
        webPageUrl <- map["webPageUrl"]
        appStoreUrl <- map["appStoreUrl"]
        actionType <- map["actionType"]
        notificationTitle <- map["notificationTitle"]
        notificationSubtitle <- map["notificationSubtitle"]
        notificationBody <- map["notificationBody"]
    }
}
