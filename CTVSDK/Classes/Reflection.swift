//
//  Reflection.swift
//  Pods
//
//  Created by Anton Zagrebelny on 28/01/2017.
//
//

import Foundation

class Reflection {
    public static func swizzle(newClass: AnyClass!,newSelector: Selector!, originalClass: AnyClass!, originalSelector: Selector!) -> Method!{
        var newMethod = class_getInstanceMethod(newClass, newSelector)
        let newMethodImp = method_getImplementation(newMethod);
        let newMethodTypeEncoding = method_getTypeEncoding(newMethod);
        
        var originalMethod = class_getInstanceMethod(originalClass, originalSelector)
        
        if (originalMethod != nil) {
            class_addMethod(originalClass, newSelector, newMethodImp, newMethodTypeEncoding);
            newMethod = class_getInstanceMethod(originalClass, newSelector);
            originalMethod = class_getInstanceMethod(originalClass, originalSelector);
            method_exchangeImplementations(originalMethod, newMethod);
        }
        else{
            class_addMethod(originalClass, originalSelector, newMethodImp, newMethodTypeEncoding);
        }
        
        return originalMethod;
    }
    
    public static func swizzleProperClass(newClass: AnyClass!,newSelector: Selector!, originalSubclasses: [AnyClass]!, originalClass: AnyClass!, originalSelector: Selector!) -> Method!{
        for subclass in originalSubclasses {
            if (isOverridesSelector(aClass: subclass, selector:originalSelector)) {
                return swizzle(newClass: newClass, newSelector: newSelector, originalClass:subclass, originalSelector:originalSelector);
            }
        }
        
        return swizzle(newClass: newClass, newSelector: newSelector, originalClass:originalClass, originalSelector:originalSelector);
    }
    
    public static func isOverridesSelector(aClass: AnyClass!, selector: Selector!) -> Bool{
        return aClass.instanceMethod(for: selector) != aClass.superclass()?.instanceMethod(for: selector);
    }
    
    public static func getSubclasses(parentClass:AnyClass!) -> [AnyClass]!{
        let expectedClassCount = objc_getClassList(nil, 0)
        let allClasses = UnsafeMutablePointer<AnyClass?>.allocate(capacity: Int(expectedClassCount))
        let autoreleasingAllClasses = AutoreleasingUnsafeMutablePointer<AnyClass?>(allClasses)
        let actualClassCount:Int32 = objc_getClassList(autoreleasingAllClasses, expectedClassCount)
        
        var result : [AnyClass] = [];
        
        for  i in 0...actualClassCount {
            var superClass = allClasses[Int(i)];
            
            repeat {
                if let aSuperClass = superClass {
                    superClass = class_getSuperclass(superClass!)
                }
            } while((superClass != nil) && superClass != parentClass);
            
            if (superClass == nil) {
                continue
            }
            
            result.append(allClasses[Int(i)]!);
        }
        
        allClasses.deallocate(capacity: Int(expectedClassCount))
        
        return result;
    }
    
    public static func getClassWithProtocolInHierarchy(searchClass: AnyClass!, protocolToFind: Protocol!) -> AnyClass? {
        if (!class_conformsToProtocol(searchClass, protocolToFind)){
            if searchClass.superclass() == nil {
                return nil;
            }
            
            if let foundClass = getClassWithProtocolInHierarchy(searchClass: searchClass.superclass(), protocolToFind: protocolToFind) {
                return foundClass
            }
            
            return searchClass
        }
        
        return searchClass
    }
}
