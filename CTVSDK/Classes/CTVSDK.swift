import Foundation
import UserNotifications
import ObjectMapper
import AVFoundation

@objc public class CTVSDK : NSObject {
    public static let instance = CTVSDK()
    
    public static let sdkVersion = "0.0.15"
    
    private var _agent: CTVAgent? = nil
    
    private override init() {
    }
    
    public func initialize(appId: String, launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        _agent = CTVAgent(appId: appId)
        _agent?.launch(launchOptions: launchOptions)
    }
    
    public func shutdown(){
        _agent = nil
    }
    
    public func informRegisteredForRemoteNotifications(deviceToken: Data) {
        _agent?.informRegisteredForRemoteNotifications(deviceToken: deviceToken)
    }
    
    public func informUserNotificationSettingsChanged() {
        _agent?.informUserNotificationSettingsChanged()
    }
    
    public func informRemoteNotificationReceived(userInfo: [AnyHashable: Any], completionHandler: @escaping (UIBackgroundFetchResult) -> Void) -> Bool {
        if userInfo["isCTV"] == nil {
            return false
        }
        
        if let agent = _agent {
            return agent.informRemoteNotificationReceived(userInfo: userInfo, completionHandler:completionHandler)
        }
        else {
            completionHandler(UIBackgroundFetchResult.newData)
            return true
        }
    }
    
    public func informLocalNotificationReceived(notification: UILocalNotification) -> Bool {
        if (notification.userInfo == nil){
            return false
        }
        
        if notification.userInfo!["isCTV"] == nil {
            return false
        }
        
        if let agent = _agent {
            return agent.informLocalNotificationReceived(notification: notification)
        }
        else {
            return true
        }
    }
}
