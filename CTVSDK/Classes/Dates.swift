//
//  Dates.swift
//  Pods
//
//  Created by Anton Zagrebelny on 19/02/2017.
//
//

import Foundation

class Dates {
    public static func deltaInSeconds(from: Date, to: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: from, to: to).second ?? 0
    }
    
    public static func durationMs(fromTime: Date, toTime: Date) -> Int{
        let durationNs = Calendar.current.dateComponents([.nanosecond], from: fromTime, to: toTime).nanosecond ?? 0
        return Int(round(Double(durationNs) / 1000000))
    }
}
