//
//  CTVAppDelegate.swift
//  Pods
//
//  Created by Anton Zagrebelny on 28/01/2017.
//
//

import Foundation


class CTVAppDelegate : NSObject{
    public static func ctvSwizzledTag(){}
    
    dynamic func setCTVDelegate(_ delegate : UIApplicationDelegate){
        let delegateClass = Reflection.getClassWithProtocolInHierarchy(searchClass: type(of: delegate), protocolToFind: UIApplicationDelegate.self)!
        let delegateSubclasses = Reflection.getSubclasses(parentClass: delegateClass)
        let newClass = CTVAppDelegate.self
        
        Reflection.swizzleProperClass(newClass: newClass, newSelector: #selector(newClass.ctvApplication(_:didRegisterForRemoteNotificationsWithDeviceToken:)),
                                      originalSubclasses: delegateSubclasses, originalClass: delegateClass, originalSelector: #selector(UIApplicationDelegate.application(_:didRegisterForRemoteNotificationsWithDeviceToken:)))
        
        Reflection.swizzleProperClass(newClass: newClass, newSelector: #selector(newClass.ctvApplication(_:didReceiveRemoteNotification:fetchCompletionHandler:)),
                                      originalSubclasses: delegateSubclasses, originalClass: delegateClass, originalSelector: #selector(UIApplicationDelegate.application(_:didReceiveRemoteNotification:fetchCompletionHandler:)))
        
        Reflection.swizzleProperClass(newClass: newClass, newSelector: #selector(newClass.ctvApplication(_:didFailToRegisterForRemoteNotificationsWithError:)),
                                      originalSubclasses: delegateSubclasses, originalClass: delegateClass, originalSelector: #selector(UIApplicationDelegate.application(_:didFailToRegisterForRemoteNotificationsWithError:)))
        
        Reflection.swizzleProperClass(newClass: newClass, newSelector: #selector(newClass.ctvApplication(_:didRegister:)),
                                      originalSubclasses: delegateSubclasses, originalClass: delegateClass, originalSelector: #selector(UIApplicationDelegate.application(_:didRegister:)))
        
        Reflection.swizzleProperClass(newClass: newClass, newSelector: #selector(newClass.ctvApplication(_:didReceive:)),
                                      originalSubclasses: delegateSubclasses, originalClass: delegateClass, originalSelector: #selector(UIApplicationDelegate.application(_:didReceive:)))
        
        self.setCTVDelegate(delegate)
    }
    
    dynamic func ctvApplication(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Logger.info(message: "Registered for remote notifications")
        
        CTVSDK.instance.informRegisteredForRemoteNotifications(deviceToken: deviceToken);
        
        if self.responds(to: #selector(self.ctvApplication(_:didRegisterForRemoteNotificationsWithDeviceToken:))) {
            self.ctvApplication(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
            return
        }
    }
    
    dynamic func ctvApplication(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Logger.info(message: "Remote notification received")
        
        let isCTV = CTVSDK.instance.informRemoteNotificationReceived(userInfo: userInfo, completionHandler: completionHandler)
        
        if !isCTV {
            if self.responds(to: #selector(self.ctvApplication(_:didReceiveRemoteNotification:fetchCompletionHandler:))) {
                self.ctvApplication(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
                return
            }
            
            completionHandler(UIBackgroundFetchResult.newData)
        }
        
    }
    
    dynamic func ctvApplication(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Logger.error(message: "Failed to register for remote notifications.", error: error)
        
        if self.responds(to: #selector(self.ctvApplication(_:didFailToRegisterForRemoteNotificationsWithError:))) {
            self.ctvApplication(application, didFailToRegisterForRemoteNotificationsWithError: error)
            return
        }
    }
    
    dynamic func ctvApplication(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        Logger.info(message: "Notification settings changed")
        
        CTVSDK.instance.informUserNotificationSettingsChanged()
        
        if self.responds(to: #selector(self.ctvApplication(_:didRegister:))) {
            self.ctvApplication(application, didRegister:notificationSettings)
            return
        }
    }
    
    dynamic func ctvApplication(_ application: UIApplication, didReceive notification: UILocalNotification) {
        Logger.info(message: "Local notification received")
        
        let isCTV = CTVSDK.instance.informLocalNotificationReceived(notification: notification)
        
        if !isCTV {
            if self.responds(to: #selector(self.ctvApplication(_:didReceive:))) {
                self.ctvApplication(application, didReceive:notification)
                return
            }
        }
    }
}

extension UIApplication {
    override open class func initialize() {
        // Prevent Xcode storyboard rendering process from crashing with custom IBDesignable Views
        // https://github.com/OneSignal/OneSignal-iOS-SDK/issues/160
        let processInfo = ProcessInfo.processInfo;
        
        if (processInfo.processName == "IBDesignablesAgentCocoaTouch") {
            return;
        }
        
        let isSwizzled = Reflection.swizzle(newClass: CTVAppDelegate.self, newSelector: Selector("ctvSwizzledTag:"), originalClass: self.classForCoder(), originalSelector: Selector("ctvSwizzledTag:"))
        
        if (isSwizzled != nil) {
            Logger.warn(message: "Already swizzled UIApplication.setDelegate.");
            return;
        }
        
        Reflection.swizzle(newClass: CTVAppDelegate.self, newSelector: Selector("setCTVDelegate:"), originalClass: UIApplication.classForCoder(), originalSelector: Selector("setDelegate:"));
    }
}
