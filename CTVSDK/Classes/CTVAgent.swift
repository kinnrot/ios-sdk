//
//  CTVAgent.swift
//  Pods
//
//  Created by Anton Zagrebelny on 19/02/2017.
//
//
import Foundation
import UserNotifications
import ObjectMapper
import AVFoundation

class CTVAgent {
    private let _apiProxy: ApiProxy
    private let _shakeSensor: MultiShakeSensor
    private let _deviceInfoProvider: DeviceInfoProvider
    private let _localStorage : LocalStorage
    private let _appId: String
    
    private var _deviceId: String? = nil
    private var _canShowNotification = false
    private var _player : AVAudioPlayer? = nil
    
    private var _currentAdBroadcast: AdBroadcast? = nil
    private var _beginHandlingShakesTask: DispatchWorkItem? = nil
    private var _endHandlingShakesTask: DispatchWorkItem? = nil
    private var _completionHandler: ((UIBackgroundFetchResult) -> Void)? = nil
    private var _deadlineTime : Date? = nil
    
    public init(appId : String!) {
        _appId = appId
        _apiProxy = ApiProxy()
        _shakeSensor = MultiShakeSensor()
        _deviceInfoProvider =  DeviceInfoProvider()
        _localStorage = LocalStorage()
        _shakeSensor.subscribeDeviceMultiShaken(onDeviceMultiShaken: self.onDeviceShaken)
    }
    
    public func launch(launchOptions: [UIApplicationLaunchOptionsKey: Any]?){
        _deviceId = _localStorage.loadDeviceId()
        
        let application = UIApplication.shared
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge],
                                                                    completionHandler: { (granted, error) in self.informUserNotificationSettingsChanged() })
        } else {
            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .sound, .badge], categories: nil))
        }
        
        application.registerForRemoteNotifications()
        
        if let notification = launchOptions?[UIApplicationLaunchOptionsKey.localNotification] as! UILocalNotification? {
            self.informLocalNotificationReceived(notification: notification)
        }
    }
    
    public func informRegisteredForRemoteNotifications(deviceToken: Data) {
        let deviceTokenText = deviceToken.reduce("", { $0 + String(format: "%02X", $1) })
        
        _deviceInfoProvider.getDeviceInfo { (deviceInfo :DeviceInfo) in
            self._apiProxy.subscribeDevice(appId: self._appId,
                                      existingDeviceId: self._deviceId,
                                      deviceToken: deviceTokenText,
                                      deviceInfo: deviceInfo,
                                      callback: {(deviceId) in
                                        self.updateDeviceId(deviceId: deviceId)
            })
        };
    }
    
    public func informUserNotificationSettingsChanged() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
                if notificationSettings.alertSetting == .enabled {
                    self._canShowNotification = true
                }
            }
        } else {
            if let notificationSettings = UIApplication.shared.currentUserNotificationSettings {
                if notificationSettings.types.contains([.alert]) {
                    _canShowNotification = true
                }
            }
        }
    }
    
    public func informRemoteNotificationReceived(userInfo: [AnyHashable: Any], completionHandler: @escaping (UIBackgroundFetchResult) -> Void) -> Bool {
        if userInfo["isCTV"] == nil {
            return false
        }
        
        if let notificationType = userInfo["type"] {
            if notificationType as! String == "HEARTBEAT" {
                Logger.info(message: "Heartbeat received")
                completionHandler(UIBackgroundFetchResult.newData)
                return true
            }
        }
        
        if let adBroadcast = _currentAdBroadcast {
            _beginHandlingShakesTask!.cancel()
            _endHandlingShakesTask!.cancel()
            endHandlingShakes()
        }
        
        let adBroadcastDto = userInfo["adBroadcast"] as! Dictionary<String, Any>
        let adBroadcast = Mapper<AdBroadcast>().map(JSON: adBroadcastDto)!
        
        sendFeedback(adBroadcastId: adBroadcast.adBroadcastId, feedbackKind: .notificationReceived)
        
        let now = ServerTime.now()
        
        if now > adBroadcast.getAdEndTime() {
            Logger.warn(message: "Received an expired ad broadcast. ad broadcast id: \(adBroadcast.adBroadcastId)")
            sendFeedback(adBroadcastId: adBroadcast.adBroadcastId, feedbackKind: .adBroadcastExpired)
            return true
        }
        
        _currentAdBroadcast = adBroadcast
        _beginHandlingShakesTask = DispatchWorkItem(block: self.beginHandlingShakes)
        _endHandlingShakesTask = DispatchWorkItem(block: self.endHandlingShakes)
        _completionHandler = completionHandler
        _deadlineTime = now.addingTimeInterval(TimeInterval(29))
        
        if now < adBroadcast.adStartTime {
            let startsInSec = Dates.deltaInSeconds(from: now, to: adBroadcast.adStartTime)
            Logger.warn(message: "Received an ad broadcast ahead of time, scheduling in \(startsInSec) seconds. ad broadcast id: \(adBroadcast.adBroadcastId)")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(min(startsInSec, 29)), execute: self._beginHandlingShakesTask!)
        }
        else {
            beginHandlingShakes()
        }
        
        return true
    }
    
    public func informLocalNotificationReceived(notification: UILocalNotification) -> Bool {
        let application = UIApplication.shared
        
        if let userInfo = notification.userInfo {
            if userInfo["isCTV"] != nil {
                let state = application.applicationState;
                
                if(state == .active){
                    if let soundUrl = Bundle.main.url(forResource: "ctv_shake_sound", withExtension: "caf"){
                        do {
                            _player = try AVAudioPlayer(contentsOf: soundUrl)
                            _player!.prepareToPlay()
                            _player!.play()
                        } catch let error {
                            Logger.warn(message: "Failed to play sound.", error: error)
                        }
                    }
                }
                
                let adBroadcastDto = userInfo["adBroadcast"] as! Dictionary<String, Any>
                let adBroadcast = Mapper<AdBroadcast>().map(JSON: adBroadcastDto)!
                let actionType = adBroadcast.actionType
                
                if actionType == "app_page" {
                    DispatchQueue.main.async {
                        application.openURL(URL(string: adBroadcast.appStoreUrl!)!)
                    }
                }
                else if actionType == "web_page" {
                    DispatchQueue.main.async {
                        application.openURL(URL(string: adBroadcast.webPageUrl!)!)
                    }
                }
                
                sendFeedback(adBroadcastId: adBroadcast.adBroadcastId, feedbackKind: .actionTriggered)
                
                return true
            }
        }
        
        return false
    }
    
    private func updateDeviceId(deviceId: String){
        self._deviceId = deviceId
        _localStorage.storeDeviceId(deviceId: deviceId)
    }
    
    private func beginHandlingShakes() {
        Logger.info(message: "Begin handling shakes")
        
        let endHandlingShakesTask = DispatchWorkItem(block: self.endHandlingShakes)
        
        _shakeSensor.start()
        
        let expiresInSec = Dates.deltaInSeconds(from: ServerTime.now(), to: _currentAdBroadcast!.getAdEndTime())
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(min(expiresInSec, 25)), execute: endHandlingShakesTask)
    }
    
    private func endHandlingShakes() {
        Logger.info(message: "End handling shakes")
        
        self._shakeSensor.stop()
        
        _completionHandler!(UIBackgroundFetchResult.newData)
        _currentAdBroadcast = nil
    }
    
    private func onDeviceShaken() {
        if _canShowNotification {
            if let adBroadcast = self._currentAdBroadcast {
                if #available(iOS 10.0, *) {
                    showLocalNotification10(adBroadcast: adBroadcast)
                } else {
                    showLocalNotification8(adBroadcast: adBroadcast)
                }
                
                _endHandlingShakesTask!.cancel()
                endHandlingShakes()
                
                sendFeedback(adBroadcastId:adBroadcast.adBroadcastId, feedbackKind: .deviceShaken)
            }
        }
    }
    
    @available(iOS 8.0, *)
    private func showLocalNotification8(adBroadcast: AdBroadcast) {
        let notification = UILocalNotification()
        
        if #available(iOS 8.2, *) {
            notification.alertTitle = adBroadcast.notificationTitle
        }
        
        notification.alertBody = adBroadcast.notificationBody
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.category = "CTV_AD_BROADCAST"
        notification.userInfo = toLocalNotificationUserInfo(adBroadcast: adBroadcast)
        notification.soundName = "ctv_shake_sound.caf"
        
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    @available(iOS 10.0, *)
    private func showLocalNotification10(adBroadcast: AdBroadcast) {
        let content = UNMutableNotificationContent()
        
        content.title = adBroadcast.notificationTitle
        if let subtitle = adBroadcast.notificationSubtitle {
            content.subtitle = subtitle
        }
        content.body = adBroadcast.notificationBody
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "CTV_AD_BROADCAST"
        content.userInfo = toLocalNotificationUserInfo(adBroadcast: adBroadcast)
        content.sound = UNNotificationSound.init(named: "ctv_shake_sound.caf")
        
        let request = UNNotificationRequest(identifier: "CTV_AD_BROADCAST", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    private func toLocalNotificationUserInfo(adBroadcast: AdBroadcast) -> [String: Any] {
        let adBroadcastDto = Mapper<AdBroadcast>().toJSON(adBroadcast)
        return [
            "isCTV": true,
            "adBroadcast": adBroadcastDto
        ]
    }
    
    private func sendFeedback(adBroadcastId: String, feedbackKind: FeedbackKind){
        if let deviceId = _deviceId {
            Logger.info(message: "Sending feedback. ad broadcast id: \(adBroadcastId), feedback kind: \(feedbackKind)")
            _apiProxy.sendFeedback(appId: _appId, deviceId: deviceId, adBroadcastId: adBroadcastId, feedbackKind: feedbackKind)
        }
        else{
            Logger.warn(message: "Cannot send feedback, no device id yet. ad broadcast id: \(adBroadcastId), feedback kind: \(feedbackKind)")
        }
    }
}
