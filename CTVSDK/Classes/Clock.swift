//
//  Clock.swift
//  Pods
//
//  Created by Anton Zagrebelny on 19/02/2017.
//
//

import Foundation

class Clock {
    private var _time : Date!;
    private var _delta: Int = 0;
    private var _isFrozen: Bool = false;
    
    public init() {
        _time = getRealNowInUtc();
    }
    
    public func freeze() {
        _isFrozen = true;
    }
    
    public func unfreeze() {
        _isFrozen = false;
    }
    
    public func setTime(time: Date!) {
        let realNowInUtc = getRealNowInUtc()
        
        _delta = Dates.deltaInSeconds(from: realNowInUtc, to: time)
        
        _time = time;
    }
    
    public func now() -> Date {
        if (_isFrozen) {
            return _time;
        }
        else {
            return getRealNowInUtc().addingTimeInterval(TimeInterval(-self._delta));
        }
    }
    
    public func reset() {
        _time = getRealNowInUtc();
        _delta = 0;
        _isFrozen = false;
    }
    
    private func getRealNowInUtc() -> Date {
        return Date() // todo: not sure if this is right implementation
    }
}
