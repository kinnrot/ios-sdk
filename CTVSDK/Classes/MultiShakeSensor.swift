//
//  MultiShakeSensor.swift
//  Pods
//
//  Created by Anton Zagrebelny on 01/03/2017.
//
//

import Foundation

class MultiShakeSensor {
    private let _shakeSensor: ShakeSensor
    
    private var _onDeviceMultiShaken :() -> Void = {}
    
    private var _firstShakeTime : Date?
    private var _lastShakeTime : Date?
    private var _shakeCount = 0
    
    public init(){
        _shakeSensor = ShakeSensor()
        _shakeSensor.subscribeDeviceShaken(onDeviceShaken: self.onDeviceShaken)
    }
    
    public func start(){
        _shakeSensor.start()
    }
    
    public func stop(){
        _shakeSensor.stop()
    }
    
    
    public func subscribeDeviceMultiShaken(onDeviceMultiShaken: @escaping () -> Void){
        _onDeviceMultiShaken = onDeviceMultiShaken
    }
    
    private func onDeviceShaken() {
        let now = Date();
        
        if _firstShakeTime == nil {
            _firstShakeTime = now;
            _lastShakeTime = now;
        }
        
        let fromLastShakeMs = Dates.durationMs(fromTime: _lastShakeTime!, toTime: now)
        
        if (fromLastShakeMs < 1500) {
            _lastShakeTime = now
            _shakeCount += 1
            
            if _shakeCount >= 2 {
                let totalDuration = Dates.durationMs(fromTime: _firstShakeTime!, toTime: now)
                
                if (totalDuration < 3000) {
                    Logger.info(message:"A MULTI SHAKE!")
                    self._onDeviceMultiShaken()
                    resetSensorState()
                }
            }
        }
        else {
            resetSensorState()
        }

    }
    
    private func resetSensorState(){
        _firstShakeTime = nil
        _lastShakeTime = nil
        _shakeCount = 0
    }
}
