//
//  ServerTime.swift
//  Pods
//
//  Created by Anton Zagrebelny on 19/02/2017.
//
//

import Foundation

class ServerTime {
    private static var _clock = Clock()
    
    public static func now() -> Date {
        return _clock.now()
    }
    
    public static func setTime(time: Date) {
        _clock.setTime(time: time)
    }
}
