//
//  DeviceInfo.swift
//  Pods
//
//  Created by Anton Zagrebelny on 12/02/2017.
//
//

import Foundation
import CoreTelephony
import AdSupport

class DeviceInfoProvider {
    private let _operationQueue = OperationQueue()
    
    public func getDeviceInfo(callback: @escaping (_: DeviceInfo) -> Void) {
        _operationQueue.addOperation({
            var systemInfo = utsname()
            uname(&systemInfo)
            
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            
            let modelName = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8, value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
            
            let osVersion = UIDevice.current.systemVersion
            
            let adId = ASIdentifierManager.shared().advertisingIdentifier?.uuidString
            
            let countryCode = CTTelephonyNetworkInfo().subscriberCellularProvider?.isoCountryCode ?? self.resolveCountryCodeByIp()
            
            let deviceInfo = DeviceInfo(adId: adId,
                                        modelName: modelName,
                                        osVersion: osVersion,
                                        countryCode: countryCode?.uppercased())
            
            callback(deviceInfo)
        })
    }
    
    private func resolveCountryCodeByIp() -> String? {
        let request = MutableURLRequest(url: URL(string: "http://ip-api.com/json")!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        var response: URLResponse?
        
        do {
            let respData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
            
            if let httpResponse = response as? HTTPURLResponse {
                if (httpResponse.statusCode >= 400) {
                    return nil
                }
            }
            
            do {
                let respContent = try JSONSerialization.jsonObject(with: respData, options: .mutableContainers) as! Dictionary<String, Any>
                let countryCode = respContent["countryCode"] as? String
                return countryCode
            } catch let error  {
                return nil
            }         }
        catch let error {
            return nil
        }
    }
}

class DeviceInfo{
    public let adId: String?
    public let modelName: String!
    public let osVersion: String!
    public let countryCode: String!
    
    init(adId: String?, modelName: String!, osVersion: String!, countryCode: String!) {
        self.adId = adId
        self.modelName = modelName
        self.osVersion = osVersion
        self.countryCode = countryCode
    }
}
