//
//  LocalStorage.swift
//  Pods
//
//  Created by Anton Zagrebelny on 07/03/2017.
//
//

import Foundation

class LocalStorage {
    private let _userDefaults = UserDefaults.standard
    private let _deviceIdKey = "ctvsdk_deviceId"
    
    public func storeDeviceId(deviceId: String){
        _userDefaults.set(deviceId, forKey: _deviceIdKey)
        _userDefaults.synchronize()
    }
    
    public func loadDeviceId() -> String? {
        return _userDefaults.string(forKey: _deviceIdKey)
    }
}
