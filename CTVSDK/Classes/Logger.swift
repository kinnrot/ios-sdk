//
//  Logger.swift
//  Pods
//
//  Created by Anton Zagrebelny on 12/02/2017.
//
//

import Foundation

class Logger {
    private static let _tag = "CTVSDK"
    
    public static func debug(message: String){
        print("\(_tag) - \(message)")
    }
    
    public static func info(message: String){
        print("\(_tag) - \(message)")
    }
    
    public static func warn(message: String){
        self.warn(message: message, error: nil)
    }
    
    public static func warn(message: String, error: Error?){
        var aMessage = "\(_tag) - \(message)"
        
        if let anError = error {
            aMessage = aMessage + ". Error: \(anError.localizedDescription)"
        }
        
        print(aMessage)
    }
    
    public static func error(message: String){
        self.error(message: message, error: nil)
    }
    
    public static func error(message: String, error: Error?){
        var aMessage = "\(_tag) - \(message)"
        
        if let anError = error {
            aMessage = aMessage + ". Error: \(anError.localizedDescription)"
        }
        
        print(aMessage)
    }
}
