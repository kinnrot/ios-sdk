//
//  AppDelegate.swift
//  CTVSDK
//
//  Created by antonzy on 12/28/2016.
//  Copyright (c) 2016 antonzy. All rights reserved.
//

import UIKit
import CTVSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("Example - Application launched")
        
//        let appId = "app-29001182413c"
        let appId = "app-7177cb936b56"
        CTVSDK.instance.initialize(appId: appId, launchOptions: launchOptions)
        
        return true
    }
}
