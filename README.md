# CTVSDK

[![CI Status](http://img.shields.io/travis/antonzy/CTVSDK.svg?style=flat)](https://travis-ci.org/antonzy/CTVSDK)
[![Version](https://img.shields.io/cocoapods/v/CTVSDK.svg?style=flat)](http://cocoapods.org/pods/CTVSDK)
[![License](https://img.shields.io/cocoapods/l/CTVSDK.svg?style=flat)](http://cocoapods.org/pods/CTVSDK)
[![Platform](https://img.shields.io/cocoapods/p/CTVSDK.svg?style=flat)](http://cocoapods.org/pods/CTVSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CTVSDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CTVSDK"
```

## Author

antonzy, antonzy90@gmail.com

## License

CTVSDK is available under the MIT license. See the LICENSE file for more info.
